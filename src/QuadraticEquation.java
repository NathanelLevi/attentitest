
public class QuadraticEquation {
	public boolean solve(String a_String, String b_String, String c_String, String x1_String, String x2_String) {
		double a,b,c, x1, x2, x1test, x2test;
		double delta;
		try {
			a = Double.parseDouble(a_String);
			b = Double.parseDouble(b_String);
			c = Double.parseDouble(c_String);
			x1 = Double.parseDouble(x1_String);
			x2 = Double.parseDouble(x2_String);
		}catch (Exception e) {
			System.err.println("Wrong params!");
			return false;
		}
		delta = Math.sqrt( Math.pow(b, 2) - (4 * a * c));
		 if (a != 0) {
	            if (delta > 0) {
	            	x1test = ((-b) - delta) / (2 * a);
	            	x2test = ((-b) + delta) / (2 * a);
	                if(x1test != x1 || x2test != x2) {
	                	x1test = ((-b) + delta) / (2 * a);
	        			x2test = ((-b) - delta) / (2 * a);
	                } 
	                if(x1 != x1test || x2 != x2test) {
	                	System.err.println("X1 or X2 or both are wrong");
	                	return false;
	        		}
	            } 
	            else if (delta == 0) {
	                x1test = (-b) / (2 * a);
	                System.out.println("x1=" + x1test);
	                return true;
	            } 
	            else {
	                System.err.println("No real roots.");
	                return false;
	            }
	        } else {
	            System.err.println("Error: division by zero.");
	            return false;
	        }
		 System.out.println("OK");
		 return true;
	}
}
