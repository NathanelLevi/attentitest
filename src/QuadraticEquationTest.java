import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class  QuadraticEquationTest {
	private String a;
	private String b;
	private String c;
	private String x1;
	private String x2;
	private QuadraticEquation equation;
	
	public QuadraticEquationTest(String a, String b, String c, String x1, String x2) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.x1 = x1;
		this.x2 = x2;
	}
	
	@Before
	public void initialize() {
		equation = new QuadraticEquation();
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		String path;
		Scanner someInput = new Scanner(System.in);  // Create a Scanner object
	    System.out.println("Enter path of csv file: \n");
	    
	    path = someInput.nextLine();
	    
		BufferedReader reader;
		List<Object[]> data = new LinkedList<Object[]>();
		if(new File(path).exists()) {
			try {
				reader = new BufferedReader(new FileReader(path));
				String line = reader.readLine();
				int i = 1;
				while (line != null) {
					System.out.println("Input "  + i + " : " +  line);
					String[] str = line.split(",");
					data.add(new Object[] {str[0],str[1],str[2], str[3],str[4]});
					// read next line
					line = reader.readLine();
					i++;
				}
				reader.close();
			} 
			catch (IOException e) {
				System.err.println("Wrong Path! exiting!");
			}
		}
		return data;
	}
	
	@Test
	public void test() {
		assertTrue(equation.solve(a, b, c, x1, x2));
	}

}

